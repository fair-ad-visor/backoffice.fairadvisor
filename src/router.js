import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
import store from "./plugins/vuex";

Vue.use(Router);

const router = new Router({
    linkExactActiveClass: 'active',
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: 'dashboard',
            component: DashboardLayout,
            meta: {
                requiresAuth: true,
            },
            children: [
                {
                    path: '/dashboard',
                    name: 'dashboard',
                    // route level code-splitting
                    // this generates a separate chunk (about.[hash].js) for this route
                    // which is lazy-loaded when the route is visited.
                    component: () => import(/* webpackChunkName: "logged-in" */ './views/Dashboard.vue'),
                    meta: {
                        requiresAdmin: true,
                    },
                },
                {
                    path: '/places',
                    name: 'places',
                    component: () => import(/* webpackChunkName: "logged-in" */ './views/Places.vue'),
                },
                {
                    path: '/places/:id',
                    name: 'place',
                    component: () => import(/* webpackChunkName: "logged-in" */ './views/Place.vue'),
                },
                {
                    path: '/profile',
                    name: 'profile',
                    component: () => import(/* webpackChunkName: "logged-in" */ './views/UserProfile.vue'),
                },
            ],
        },
        {
            path: '/',
            redirect: 'login',
            component: AuthLayout,
            children: [
                {
                    path: '/login',
                    name: 'login',
                    component: () => import(/* webpackChunkName: "logged-out" */ './views/Login.vue'),
                },
                {
                    path: '/register',
                    name: 'register',
                    component: () => import(/* webpackChunkName: "logged-out" */ './views/Register.vue'),
                },
            ],
        },
    ],
});

router.beforeEach(async (to, from, next) => {
    // Wait for VueX-Persist to load the store
    await store.restored;

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.state.access_token) {
            if (to.matched.some(record => record.meta.requiresAdmin) && store.state.user_infos.user_type.id !== 3) {
                next({name: 'places'});
            } else next();
        } else next({name: 'login'});
    } else {
        next();
    }
})

export default router;
