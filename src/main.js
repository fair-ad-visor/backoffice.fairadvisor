import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/axios'
import './plugins/leaflet'
import './plugins/vue-spinner'
import './plugins/slide-up-down'
import './plugins/sweetalert2'
import store from './plugins/vuex'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'

Vue.config.productionTip = false;

Vue.use(ArgonDashboard);

new Vue({
    router,
    store,
    beforeCreate() {
        this.$store.commit('initialiseStore');
    },
    render: h => h(App),
}).$mount('#app');
