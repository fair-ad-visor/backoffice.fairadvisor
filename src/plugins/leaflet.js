import Vue from 'vue';
import {LMap, LTileLayer, LMarker, LPopup, LIcon} from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';

//region Fix default leaflet maker load error
import L from 'leaflet';
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
//endregion

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-icon', LIcon);
Vue.component('l-marker', LMarker);
Vue.component('l-popup', LPopup);
