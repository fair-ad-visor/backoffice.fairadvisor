import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from "./vuex";
import router from "../router";

Vue.use(VueAxios, axios);

const baseURL = process.env.NODE_ENV === 'development' ? process.env.VUE_APP_API_URI_DEV : process.env.VUE_APP_API_URI_PROD;

if (typeof baseURL !== 'undefined') {
    Vue.axios.defaults.baseURL = baseURL;
}

// Redirect user on login page in case he gets a 401 response from the api
axios.interceptors.response.use(
    (response) => response,
    async (response) => {
        // Wait for VueX-Persist to load the store
        await store.restored;

        if (response.response.status === 401) {
            store.commit('setAccessToken', undefined);
            router.push({name: 'login'});
        }

        return Promise.reject(response);
    },
);
