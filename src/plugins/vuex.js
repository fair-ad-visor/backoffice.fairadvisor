import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        access_token: null,
        user_infos: null,
    },
    mutations: {
        setAccessToken(state, access_token) {
            state.access_token = access_token;
            axios.defaults.headers.common.Authorization = `Bearer ${access_token}`;
        },
        setUserInfos(state, user_infos) {
            state.user_infos = user_infos;
        },
        initialiseStore(state) {
            if (localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );

                axios.defaults.headers.common.Authorization = `Bearer ${state.access_token}`;
            }
        },
    },
    getters: {}
});

store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});

export default store;
