import Vue from 'vue'

import GridLoader from 'vue-spinner/src/GridLoader.vue'
import ScaleLoader from 'vue-spinner/src/ScaleLoader.vue'

Vue.component('grid-loader', GridLoader);
Vue.component('scale-loader', ScaleLoader);
