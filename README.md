<p align="center">
    <img src="https://imgur.com/K5Oi6uc.png" height="100">
    <img src="https://vuejs.org/images/logo.png" width="100">
</p>

## 🛠 How to setup

First copy the **.env** into **.env.local**

Run

```shell
npm install
```



Then start filling the **.env.local** files database and oauth services variables

```
VUE_APP_API_URI_DEV=
VUE_APP_API_URI_PROD=
```


After that you can start the project with

```shell
npm run serve
```



## 📜 License

The VueJS framework and this repository are open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
